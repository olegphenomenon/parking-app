package E2E;

import UI.HarlayPontNeufPage;
import UI.TheBookingMapPage;
import UI.TheMainPage;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class E2ETest {
    private String userName = System.getProperty("username");
    private String password = System.getProperty("password");

    WebDriver driver;
    WebDriverWait wait;

    TheMainPage mainPage;
    TheBookingMapPage theBookingMapPage;
    HarlayPontNeufPage harlayPontNeufPage;

    String email;

    Date date;
    DateFormat dateFormat;

    public void login(String userName, String password) {
        driver.get("https://" + userName + ":" + password + "@dev.opngo.com/en");
    }

    public String convertDateToString() {
        date = Calendar.getInstance().getTime();
        dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
        return dateFormat.format(date).replace("-", "").replace(" ", "").replace(":", "");
    }

    @Before
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "C:\\drivers\\chromedriver.exe");
        driver = new ChromeDriver();
        wait = new WebDriverWait(driver, 5);
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

        email = convertDateToString() + "@test.com";

        mainPage = new TheMainPage(driver);
        theBookingMapPage = new TheBookingMapPage(driver);
        harlayPontNeufPage = new HarlayPontNeufPage(driver);
    }

    @Test
    public void E2ETest() {
        login(userName, password);

        //Close cookie privacy window
        driver.switchTo().activeElement();
        driver.findElement(By.xpath("//div[@class='button button-primary cookie-accept-all']")).click();

        //Step 1
        mainPage.typeLocation("Harlay Pont Neuf");
        //Step 2
        mainPage.clickToTheDropDownMenuResult("Harlay Pont Neuf");
        //Step 3
        theBookingMapPage.clickOnTheFirstButtonFromTheHarlayNeufList();
        //Step 4
        harlayPontNeufPage.clickOnTheListOfOffers();
        harlayPontNeufPage.chooseElementOfOffersList("1 hour");
        //Step 5
        harlayPontNeufPage.clickOnTheBookNowButton();
        //Step 6, 7, 8
        harlayPontNeufPage.typeFirstName("Oleg").typeLastName("Hasjanov").typePhoneNumber("+372223223");
        //Step 9
        harlayPontNeufPage.clickOnTheCountriesList();
        harlayPontNeufPage.typeCountry("Estonia");
        //Step 10
        harlayPontNeufPage.typeEmail(email);
        //Step 11
        harlayPontNeufPage.typePassword("s0m3Passw0rd");
        //Step 12
        harlayPontNeufPage.clickOnTheRememberCheckBox();
        //Step 13
        harlayPontNeufPage.clickOnTheTermsCheckBox();
        //Step 14
        harlayPontNeufPage.clickOnTheCreateAnAccountButton();
        //Step 15
        harlayPontNeufPage.typeLicensePlate(convertDateToString());
        //Step16
        harlayPontNeufPage.clickOnTheAddLicensePlateButton();
        //Step 17
        harlayPontNeufPage.typeCardNumber("2321235435537663");
        //Step 18
        harlayPontNeufPage.typeNameOfCardHolder("Oleg Hasjanov");
        //Step 19
        harlayPontNeufPage.typeExpirationCardDate("04/21");
        //Step 20
        harlayPontNeufPage.typeCVVCard("432");
        //Step 21
        harlayPontNeufPage.clickOnTheReccuringPaymentToggle();
        //Step 22
        harlayPontNeufPage.clickOnTheAddPaymentButton();
    }

    @After
    public void tearDown() {
        driver.quit();
    }


}
