package UI;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

public class HarlayPontNeufPage {

    WebDriver driver;

    //checkout form
    private By listOfOffers = By.xpath("//div[@class='Select-value']");
    private String elementsOfOffersList = "//span[@class='option-label' and contains(text(), '%s')]";
    private By bookNowButton = By.xpath("//div[@class='button button-primary']");

    //Create new user form
    private By firstNameField = By.id("firstname");
    private By lastNameField = By.id("lastname");
    private By phoneNumbberField = By.id("phone");

    private By countriesList = By.xpath("//div[@class='Select select-country is-searchable Select--single']");
    private By selectCountry = By.xpath("//input[@aria-activedescendant='react-select-3--option-0']");

    private By emailField = By.id("email");
    private By passwordField = By.id("password");
    private By rememberMeCheckBox = By.xpath("//span[contains(text(),\"Remember\")]//parent::div");
    private By termsAndConditionsCheckBox = By.xpath("//span[contains(text(),\"I have read\")]//parent::div");
    private By createAnAccountButton = By.xpath("//button[@class='btn btn-default']");

    //Register vehicle number
    private By licensePlateInput = By.id("numberplate");
    private By registerAVehicleButton = By.xpath("//button[contains(text(), 'Add')]");

    //Add payment data
    private By cardNumberField = By.name("cardNumber");
    private By nameOfCardHolderField = By.name("CN");
    private By expirationCardDateField = By.name("ED");
    private By CVVField = By.name("CVC");

    private By reccuringPaymentToggle = By.xpath("//div[@class='switch toggle-switch recurring-payment-toggle  ']");
    private By addPaymentDataButton = By.xpath("//div[@class='button button-primary']");

    public HarlayPontNeufPage(WebDriver driver) {
        this.driver = driver;
    }

    //checkout form

    public HarlayPontNeufPage clickOnTheListOfOffers() {
        driver.findElement(listOfOffers).click();
        return this;
    }

    public HarlayPontNeufPage chooseElementOfOffersList(String offer) {
        driver.findElement(By.xpath(String.format(elementsOfOffersList, offer))).click();
        return this;
    }

    public HarlayPontNeufPage clickOnTheBookNowButton() {
        driver.findElement(bookNowButton).click();
        return this;
    }

    //Create new user form

    public HarlayPontNeufPage typeFirstName(String name) {
        driver.findElement(firstNameField).sendKeys(name);
        return this;
    }

    public HarlayPontNeufPage typeLastName(String lastname) {
        driver.findElement(lastNameField).sendKeys(lastname);
        return this;
    }

    public HarlayPontNeufPage typePhoneNumber(String phone) {
        driver.findElement(phoneNumbberField).sendKeys(phone);
        return this;
    }

    public HarlayPontNeufPage clickOnTheCountriesList() {
        driver.findElement(countriesList).click();
        return this;
    }

    public HarlayPontNeufPage typeCountry(String country) {
        driver.findElement(selectCountry).sendKeys(country, Keys.ENTER);
        return this;
    }

    public HarlayPontNeufPage typeEmail(String email) {
        driver.findElement(emailField).sendKeys(email);
        return this;
    }

    public HarlayPontNeufPage typePassword(String password) {
        driver.findElement(passwordField).sendKeys(password);
        return this;
    }

    public HarlayPontNeufPage clickOnTheRememberCheckBox() {
        driver.findElement(rememberMeCheckBox).click();
        return this;
    }

    public HarlayPontNeufPage clickOnTheTermsCheckBox() {
        driver.findElement(termsAndConditionsCheckBox).click();
        return this;
    }

    public HarlayPontNeufPage clickOnTheCreateAnAccountButton() {
        driver.findElement(createAnAccountButton).click();
        return this;
    }

    //Register vehicle number

    public HarlayPontNeufPage typeLicensePlate(String plate) {
        driver.findElement(licensePlateInput).sendKeys(plate);
        return this;
    }

    public HarlayPontNeufPage clickOnTheAddLicensePlateButton() {
        driver.findElement(registerAVehicleButton).click();
        return this;
    }

    //Add payout data
    public HarlayPontNeufPage typeCardNumber(String number) {
        driver.findElement(cardNumberField).sendKeys(number);
        return this;
    }

    public HarlayPontNeufPage typeNameOfCardHolder(String name) {
        driver.findElement(nameOfCardHolderField).sendKeys(name);
        return this;
    }

    public HarlayPontNeufPage typeExpirationCardDate(String date) {
        driver.findElement(expirationCardDateField).sendKeys(date);
        return this;
    }

    public HarlayPontNeufPage typeCVVCard(String cvv) {
        driver.findElement(CVVField).sendKeys(cvv);
        return this;
    }

    public HarlayPontNeufPage clickOnTheReccuringPaymentToggle() {
        driver.findElement(reccuringPaymentToggle).click();
        return this;
    }

    public HarlayPontNeufPage clickOnTheAddPaymentButton() {
        driver.findElement(addPaymentDataButton).click();
        return this;
    }


}
