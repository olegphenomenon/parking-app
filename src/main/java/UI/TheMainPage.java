package UI;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class TheMainPage {
    private By inputPlaceSearch = By.xpath("//input[@class='search-suggest__input']");
    private String dropdownMenuResult = "//p[@class='name' and text()=\"%s\"]";

    WebDriver driver;

    public TheMainPage(WebDriver driver) {
        this.driver = driver;
    }

    public TheMainPage typeLocation(String location) {
        driver.findElement(inputPlaceSearch).clear();
        driver.findElement(inputPlaceSearch).sendKeys(location);
        return this;
    }

    public TheMainPage clickToTheDropDownMenuResult(String result) {
        //p[@class='name' and text()='Harlay Pont Neuf']
        driver.findElement(By.xpath(String.format(dropdownMenuResult, result))).click();
        return this;
    }


}
