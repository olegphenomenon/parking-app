package UI;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class TheBookingMapPage {

    WebDriver driver;

    private String rateCardPrice = "//span[@class='rate-card-price' and contains(text(),'%s')";

    private By firstButtonFromTheHarlayNeufList = By.xpath("//div[@class='rate-card']");

    public TheBookingMapPage(WebDriver driver) {
        this.driver = driver;
    }

    public TheBookingMapPage clickOnTheRateCardPrice(String priceOfCard) {
        driver.findElement(By.xpath(String.format(rateCardPrice, priceOfCard))).click();
        return this;

    }

    public WebElement getTheFirstButtonFromTheHarlayNeufLisst() {
        return driver.findElement(firstButtonFromTheHarlayNeufList);
    }

    public TheBookingMapPage clickOnTheFirstButtonFromTheHarlayNeufList() {
        driver.findElement(firstButtonFromTheHarlayNeufList).click();
        return this;
    }

}
